<?php /** @noinspection PhpPropertyOnlyWrittenInspection */

namespace Apeisia\PdfUnite;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

abstract class PdfSeparate
{
    public static function separate(string $file, string $destinationDirectory): void
    {
        $process = new Process(
            [
                './pdfseparate',
                $file,
                rtrim($destinationDirectory, '/') . '/%d.pdf'
            ],
            __DIR__
        );
        $process->run();


        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }
}
