<?php /** @noinspection PhpPropertyOnlyWrittenInspection */

namespace Apeisia\PdfUnite;

use Apeisia\BaseBundle\Service\PdfUtils;

/** @deprecated Use PdfUtils from base bundle instead */
abstract class PdfUnite extends PdfUtils
{

}
